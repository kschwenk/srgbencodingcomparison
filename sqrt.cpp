// [[file:main.org::*Verification][Verification:6]]
#include "shared.hpp"
#include "sqrt.hpp"






int main() {
run([](float f){ return sqrt_(f); });
return 0;
}
// Verification:6 ends here
