// [[file:main.org::*Verification][Verification:18]]
#include "shared.hpp"
#include "exact.hpp"






int main() {
run([](float f){ return exact(f); });
return 0;
}
// Verification:18 ends here
