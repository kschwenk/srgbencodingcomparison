// [[file:main.org::*Verification][Verification:6]]
#include "shared.hpp"

alignas(64) constexpr inline std::array g_fsqrt_table =
{
#include "fsqrt_table.txt"
};

auto fsqrt(float f) -> uint8
{
    // constexpr uint32 L = 1u << 23u;
    // constexpr uint32 B = 127;
    // constexpr float s = 0.0450466f;
    constexpr uint32 magic = 0x1fbd1df4;//static_cast<uint32>(0.5f*(B-s)*L);

    // TODO(ks): replace with std::bit_cast
    uint32 const k = reinterpret_cast<uint32 const&>(f);
    uint32 const sqrtk = (k >> 1) + magic;
    float const sqrtf = reinterpret_cast<float const&>(sqrtk);

    constexpr array_index max = std::size(g_fsqrt_table) - 1;
    auto const i = quantize<array_index>(sqrtf, max);
    return g_fsqrt_table[i];
}
// Verification:6 ends here
