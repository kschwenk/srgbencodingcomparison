# Evaluation of a Few Dumb Ideas About sRGB Encoding

This project evaluates a few methods to encode gamma compressed 8-bit sRGB colors from 32-bit linear floating point colors.
You can look at the [html export](./main.html) to view the results, or at the [Org file](./main.org) to play around with the code.


## License

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/)

[CC0 1.0 Universal public domain dedication](CC0 1.0 Universal public domain dedication):
To the extent possible under law, the authors have waived all copyright and related or neighboring rights to this project.
This work is published from: Germany.
