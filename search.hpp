// [[file:main.org::*Search][Search:1]]
#include "shared.hpp"

inline auto search(float f) -> uint8
{
#if 1 // 1: binary search; 0: linear search (for the lolz)
    auto upper = std::upper_bound(std::begin(g_decode_table),
                                  std::end(g_decode_table),
                                  f);
#else
    auto upper = std::find_if(std::begin(g_decode_table),
                              std::end(g_decode_table),
                              [f](float c) {return c > f;});
#endif
    return static_cast<uint8>(upper-std::begin(g_decode_table)-1);
}
// Search:1 ends here
