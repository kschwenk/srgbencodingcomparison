// [[file:main.org::*Verification][Verification:7]]
#include "shared.hpp"
#include "fsqrt.hpp"






int main() {
run([](float f){ return fsqrt(f); });
return 0;
}
// Verification:7 ends here
