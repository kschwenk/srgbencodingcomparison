// [[file:main.org::*Verification][Verification:5]]
#include "shared.hpp"

alignas(64) constexpr inline std::array g_straight_table =
{
#include "straight_table.txt"
};

auto straight(float f) -> uint8
{
    constexpr array_index max = std::size(g_straight_table) - 1;
    auto const i = quantize<array_index>(f, max);
    return g_straight_table[i];
}
// Verification:5 ends here
