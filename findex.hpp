// [[file:main.org::*Verification][Verification:5]]
#include "shared.hpp"

alignas(64) constexpr inline std::array g_findex_table =
{
#include "findex_table.txt"
};

inline auto findex(float x) -> uint
{
    if (!(x > std::numeric_limits<float>::min())) return 0;
    if (x >= 1.0f) return 511;

    constexpr uint32 B = 127;
    constexpr uint32 magic = 0x1fbd1df4; // same as above

    // TODO(ks): replace with std::bit_cast
    uint32 const k = reinterpret_cast<uint32 const&>(x);
    uint32 const sqrtk = (k >> 1) + magic;
    uint32 const s = uint32(B+14) - uint32((sqrtk >> 23) & 0x000000FF);
    uint32 m = (sqrtk & 0x007fffff) | 0x00800000;
    uint32 i = m >> s;
    return g_findex_table[i];
}
// Verification:5 ends here
