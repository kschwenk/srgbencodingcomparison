// [[file:main.org::*Verification][Verification:4]]
#include "shared.hpp"

inline auto cast(float f) -> uint8
{
    return static_cast<uint8>(f*255.99f);
}
// Verification:4 ends here
