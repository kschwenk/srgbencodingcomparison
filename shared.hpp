// [[file:main.org::*Verification][Verification:9]]
#ifndef SHARED_HPP
#define SHARED_HPP

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::includes_and_usings][includes_and_usings]]][includes_and_usings]]
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>

using uint8 = std::uint8_t;
using uint32 = std::uint32_t;
using array_index = std::size_t;
// includes_and_usings ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::quantization_functions][quantization_functions]]][quantization_functions]]
template<typename U>
constexpr auto quantize(float f, U max = std::numeric_limits<U>::max()) -> U
{
    if (!(f > 0.0f))
    {
        return 0;
    }

    if (f >= 1.0f)
    {
        return max;
    }

    return static_cast<U>(f * max + 0.5f);
}

template<typename U>
constexpr auto dequantize(U u, U max = std::numeric_limits<U>::max()) -> float
{
    return u * (1.0f/max);
}
// quantization_functions ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::exact_conversions][exact_conversions]]][exact_conversions]]
inline auto gamma_decode(float u) -> float
{
    return u < 0.04045f ? 0.0773993808f * u
                        : std::pow(0.94786729857f * u + 0.05213270142f, 2.4f);
}

inline auto gamma_encode(float u) -> float
{
    return u < 0.0031308f ? 12.92f * u
                          : 1.055f * std::pow(u, 0.41666666666f) - 0.055f;
}

inline auto decode_exact(uint8 u) -> float
{
    float const f = dequantize<uint8>(u);
    return gamma_decode(f);
}

inline auto encode_exact(float f) -> uint8
{
    float const ff = gamma_encode(f);
    return quantize<uint8>(ff);
}
// exact_conversions ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::decode_lookup][decode_lookup]]][decode_lookup]]
alignas(64) constexpr std::array g_decode_table =
{
#include "decode_table.txt"
};

inline auto decode_lookup(uint8 u) -> float
{
    return g_decode_table[u];
}
// decode_lookup ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::verify_function][verify_function]]][verify_function]]
template<typename Encoder>
void verify(Encoder encoder)
{
    for (unsigned i = 0; i < std::size(g_decode_table); ++i)
    {
        float const f = g_decode_table[i];
        uint8 const u = encoder(f);
        if (u != i)
        {
            std::cerr << "Failed verification for f == " << f
                << ": expected " << i << ", got " << static_cast<int>(u) << '\n';
        }
#if 0 // 1: print passed tests as well, useful for checking that all values are tested
        else
        {
            std::cerr << "Passed verification for f == " << f
                << ": expected " << i << ", got " << static_cast<int>(u) << '\n';
        }
#endif
    }
}
// verify_function ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::inline_random_data][inline_random_data]]][inline_random_data]]
alignas(64) std::array g_random_encoded_data =
{
#include "random_encoded_data.txt"
};

alignas(64) std::array g_random_decoded_data =
{
#include "random_decoded_data.txt"
};

// inline_random_data ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::bench_random_function][bench_random_function]]][bench_random_function]]
template<typename Encoder>
void bench_random(Encoder encoder)
{
    constexpr int N = 3'000'000;
    for (int i = 0; i < N; ++i)
    {
        std::transform(std::begin(g_random_decoded_data),
                       std::end(g_random_decoded_data),
                       std::begin(g_random_encoded_data),
                       encoder);
    }
}
// bench_random_function ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::read_image_function][read_image_function]]][read_image_function]]
inline auto read_image(std::filesystem::path const& path) -> std::vector<uint8>
{
    std::vector<uint8> result;
    std::ifstream ifs(path, std::ios::binary | std::ios::ate);
    if(ifs)
    {
        std::streamoff size = ifs.tellg();
        ifs.seekg(0);
        result.resize(size);
        ifs.read(reinterpret_cast<char*>(result.data()), result.size());
        // assert(ifs.gcount() == result.size());
    }

    return result;
}
// read_image_function ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::bench_image_function][bench_image_function]]][bench_image_function]]
template<typename Encoder>
void bench_image(Encoder encoder)
{
    std::vector<uint8> image_srgb = read_image("bench.raw");
    std::vector<float> image_float(image_srgb.size());
    std::transform(std::begin(image_srgb),
                   std::end(image_srgb),
                   std::begin(image_float),
                   [](uint8 u){ return decode_lookup(u); });
    constexpr int N = 30;
    for (int i = 0; i < N; ++i)
    {
        std::transform(std::begin(image_float),
                       std::end(image_float),
                       std::begin(image_srgb),
                       encoder);
    }
}
// bench_image_function ends here

// [[[[file:~/Projects/SRGBEncodingComparison/main.org::run_function][run_function]]][run_function]]
#define RUN_nothing 0
#define RUN_verify 1
#define RUN_bench_random 2
#define RUN_bench_image 3

#if !defined(RUN_CONFIGURATION)
#define RUN_CONFIGURATION RUN_nothing
#endif

template<typename Encoder>
void run(Encoder encoder)
{
#if RUN_CONFIGURATION==RUN_verify
    verify(encoder);
#elif RUN_CONFIGURATION==RUN_bench_random
    bench_random(encoder);
#elif RUN_CONFIGURATION==RUN_bench_image
    bench_image(encoder);
#endif
}
// run_function ends here

#endif
// Verification:9 ends here
