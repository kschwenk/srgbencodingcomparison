// [[file:main.org::*Verification][Verification:6]]
#include "shared.hpp"
#include "findex.hpp"






int main() {
run([](float f){ return findex(f); });
return 0;
}
// Verification:6 ends here
