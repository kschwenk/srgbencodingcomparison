// [[file:main.org::*Verification][Verification:5]]
#include "shared.hpp"

alignas(64) constexpr inline std::array g_poly_table =
{
#include "poly_table.txt"
};

auto poly(float f) -> uint8
{
    constexpr auto p = [](float x) { return -x*x + 2.08f*x; };
    constexpr auto norm = 0.92592592592f; // 1.0f/p(1.0f);
    constexpr array_index max = std::size(g_poly_table) - 1;
    auto const i = quantize<array_index>(norm*p(f), max);
    return g_poly_table[i];
}
// Verification:5 ends here
