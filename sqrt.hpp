// [[file:main.org::*Verification][Verification:5]]
#include "shared.hpp"

alignas(64) constexpr inline std::array g_sqrt_table =
{
#include "sqrt_table.txt"
};

auto sqrt_(float f) -> uint8
{
    constexpr array_index max = std::size(g_sqrt_table) - 1;
    auto const i = quantize<array_index>(std::sqrt(f), max);
    return g_sqrt_table[i];
}
// Verification:5 ends here
