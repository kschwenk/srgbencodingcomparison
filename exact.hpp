// [[file:main.org::*Verification][Verification:8]]
#include "shared.hpp"

inline auto exact(float f) -> uint8
{
    return encode_exact(f);
}
// Verification:8 ends here
